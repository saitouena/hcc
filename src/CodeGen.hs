module CodeGen
  (
    compile,
    makeVarMap
  ) where

import Prelude hiding (exp)
import Parser -- hiding (parseC, parseCFromFile)
import Data.List (intercalate)
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.List as List
import Control.Monad.State
import Control.Monad.Trans.Except

data Asm = Directive String
         | Label String
         | Inst String
         deriving (Show)

type Env = Map.Map String Int
type Scope = Set.Set String

data CTInfo = CTInfo { env :: Env, si :: Int, labelCount :: Int, currentScope :: Scope, continueTo :: Maybe String, breakTo :: Maybe String }

wordsize :: Int
wordsize = 8

emptyEnv :: Env
emptyEnv = Map.empty

freshCTInfo :: CTInfo
freshCTInfo = CTInfo { env = Map.empty, si = -wordsize , labelCount = 0, currentScope = emptyScope, continueTo = Nothing, breakTo = Nothing }

insertVar :: String -> Compile
insertVar var = do
  ctinfo <- get
  let oldenv = env ctinfo
  let oldsi = si ctinfo
  let oldScope = currentScope ctinfo
  let newenv = Map.insert var oldsi oldenv
  let newsi = oldsi - wordsize
  let newScope = Set.insert var oldScope
  let lc = labelCount ctinfo
  put $ CTInfo { env = newenv, si = newsi, labelCount = lc, currentScope = newScope, continueTo = continueTo ctinfo, breakTo = breakTo ctinfo }
  return []

incrLabelCount :: Compile
incrLabelCount = do
  ctinfo <- get
  let old = labelCount ctinfo
  let new = old+1
  put $ CTInfo { env = env ctinfo, si = si ctinfo, labelCount = new, currentScope = currentScope ctinfo, continueTo = continueTo ctinfo, breakTo = breakTo ctinfo }
  return []

makeLabel :: Int -> String
makeLabel lc = "L" ++ show lc

emptyScope :: Scope
emptyScope = Set.empty

putNewScope :: Compile
putNewScope = do
  ctinfo <- get
  put $ CTInfo { env = env ctinfo, si = si ctinfo, labelCount = labelCount ctinfo, currentScope = emptyScope, continueTo = continueTo ctinfo, breakTo = breakTo ctinfo }
  return []

setBreakTo :: String -> Compile
setBreakTo label = do
  ctinfo <- get
  put $ CTInfo { env = env ctinfo, si = si ctinfo, labelCount = labelCount ctinfo, currentScope = emptyScope, continueTo = continueTo ctinfo, breakTo = Just label }
  return []

resetBreakTo :: Maybe String -> Compile
resetBreakTo label = do
  ctinfo <- get
  put $ CTInfo { env = env ctinfo, si = si ctinfo, labelCount = labelCount ctinfo, currentScope = emptyScope, continueTo = continueTo ctinfo, breakTo = label}
  return []

setContinueTo :: String -> Compile
setContinueTo label = do
  ctinfo <- get
  put $ CTInfo { env = env ctinfo, si = si ctinfo, labelCount = labelCount ctinfo, currentScope = emptyScope, continueTo = Just label, breakTo = breakTo ctinfo }
  return []

resetContinueTo :: Maybe String -> Compile
resetContinueTo label = do
  ctinfo <- get
  put $ CTInfo { env = env ctinfo, si = si ctinfo, labelCount = labelCount ctinfo, currentScope = emptyScope, continueTo = label, breakTo = breakTo ctinfo }
  return []

setArgs :: [String] -> Compile
setArgs args = do
  ctInfo <- get
  let initEnv = makeVarMap args (wordsize*2)
  let initScope = makeScope args
  if length args == (length $ List.nub $ List.sort args)
    then (put $ CTInfo { env = initEnv, si = si ctInfo, labelCount = labelCount ctInfo, currentScope = initScope, continueTo = continueTo ctInfo, breakTo = breakTo ctInfo }) >> return []
    else throwE "duplicate args" >> return []

makeVarMap :: [String] -> Int -> Env
makeVarMap args base = loop args base emptyEnv
                          where loop [] _ e = e
                                loop (a:args') offset e = loop args' (offset+wordsize) (Map.insert a offset e)

makeScope :: [String] -> Scope
makeScope args = foldr (\a -> \s -> Set.insert a s) emptyScope args

type Compile = ExceptT String (State CTInfo) [Asm]

-- runCompile :: Compile -> CTInfo -> (Either String [Asm], CTInfo)
-- runCompile prog = runState $ runExceptT

-- emitDebug :: String -> [Asm]
-- emitDebug filename = case parseCFromFile filename of
--                        Right p -> emit p
--                        _ -> undefined

emit :: Prog -> Either String [Asm]
emit (Prog fndecls) = fst $ (runState $ runExceptT $ emitFundecls fndecls) freshCTInfo

emitFundecls :: [Fundecl] -> Compile
emitFundecls [] = return []
emitFundecls (fundecl:rest) = do
  a1 <- emitFundecl' fundecl
  a2 <- emitFundecls rest
  return $ a1 ++ a2

emitFundecl' :: Fundecl -> Compile
emitFundecl' (Function fname args (Just items)) = do
  let header = [Directive $ ".globl " ++ fname, Label $ "" ++ fname]
  let pro = [ Inst "push %rbp", Inst "movq %rsp, %rbp"]
  let epi = [ Inst "movq %rbp, %rsp", Inst "pop %rbp", Inst "movq $0, %rax", Inst "ret" ]
  void $ setArgs args
  a <- emitFundecl items
  return $ header ++ pro ++ a ++ epi
emitFundecl' (Function _ _ Nothing) = return []

emitFundecl :: [BlockItem] -> Compile
emitFundecl [] = return []
emitFundecl (item:rest) = do
  a1 <- emitBlockItem item
  a2 <- emitFundecl rest
  return $ a1 ++ a2

emitBlockItem :: BlockItem -> Compile
emitBlockItem item = case item of
                      ItemStmt stmt -> emitStmt stmt
                      ItemDeclaration dcl -> emitDeclaration dcl

emitDeclaration :: Declaration -> Compile
emitDeclaration (Declaration var Nothing) = do
  ctinfo <- get
  let cs = currentScope ctinfo
  if Set.member var cs
    then throwE $ "redefinition of variable in the same scope: var=" ++ var
    else insertVar var >> (return $ [ Inst $ "push %rax" ])
emitDeclaration (Declaration var (Just exp)) = do
  ctinfo <- get
  let cs = currentScope ctinfo
  a <- emitExp exp
  if Set.member var cs
    then throwE $ "redefinition of variable in the same scope: var=" ++ var
    else insertVar var >> (return $ a ++ [ Inst $ "push %rax" ])

emitStmt :: Stmt -> Compile
emitStmt (Return e) = do
  a <- emitExp e
  return $ a ++ [ Inst "movq %rbp, %rsp", Inst "pop %rbp", Inst "ret" ]
emitStmt (StmtExp (Just e)) = do
  a <- emitExp e
  return a
emitStmt (StmtExp Nothing) = return []
emitStmt (ConditionalStmt cond thenStmt Nothing) = do
  ctinfo <- get
  let lc = labelCount ctinfo
  let l = makeLabel lc
  void $ incrLabelCount
  a1 <- emitExp cond
  a2 <- emitStmt thenStmt
  return $ a1 ++ [ Inst "cmpq $0, %rax", Inst $ "je " ++ l ] ++ a2 ++ [ Label l ]
emitStmt (ConditionalStmt cond thenStmt (Just elseStmt)) = do
  ctinfo <- get
  let lc1 = labelCount ctinfo
  let lc2 = 1 + labelCount ctinfo
  let l1 = makeLabel lc1
  let l2 = makeLabel lc2
  void $ incrLabelCount
  void $ incrLabelCount
  a1 <- emitExp cond
  a2 <- emitStmt thenStmt
  a3 <- emitStmt elseStmt
  return $ a1 ++ [Inst $ "cmpq $0, %rax", Inst $ "je " ++ l1] ++ a2 ++ [ Inst $ "jmp " ++ l2 ] ++ [ Label l1 ] ++ a3 ++ [ Label l2 ]
emitStmt (Compound items) = do
  oldctinfo <- get
  void $ putNewScope
  loop items 0 [] oldctinfo
  where loop [] count asm oldctinfo = do
          newctinfo <- get
          put $ CTInfo { env = env oldctinfo, si = si oldctinfo, labelCount = labelCount newctinfo, currentScope = currentScope oldctinfo, continueTo = continueTo oldctinfo, breakTo = breakTo oldctinfo}
          return $ asm ++ [ Inst $ "addq $" ++ show (wordsize*count) ++ ", %rsp" ]
        loop (item@(ItemStmt _):rest) count asm oldctinfo = do
          a <- emitBlockItem item
          loop rest count (asm ++ a) oldctinfo
        loop (item@(ItemDeclaration _):rest) count asm oldctinfo = do
          a <- emitBlockItem item
          loop rest (1+count) (asm++a) oldctinfo
emitStmt (While e stmt) = do
  ctInfo <- get
  let l1 = makeLabel $ labelCount ctInfo
  let l2 = makeLabel $ 1+labelCount ctInfo
  void $ incrLabelCount
  void $ incrLabelCount
  void $ setBreakTo l2
  void $ setContinueTo l1
  a1 <- emitExp e
  a2 <- emitStmt stmt
  void $ resetContinueTo $ continueTo ctInfo
  void $ resetBreakTo $ breakTo ctInfo
  return $ [Label l1] ++ a1 ++ [Inst "cmpq $0, %rax", Inst $ "je " ++ l2] ++ a2 ++ [ Inst $ "jmp " ++ l1 , Label l2 ]
emitStmt (Do stmt e) = do
  ctInfo <- get
  let l1 = makeLabel $ labelCount ctInfo
  let l2 = makeLabel $ 1+labelCount ctInfo
  void $ incrLabelCount
  void $ incrLabelCount
  void $ setBreakTo l2
  void $ setContinueTo l1
  a1 <- emitStmt stmt
  a2 <- emitExp e
  void $ resetContinueTo $ continueTo ctInfo
  void $ resetBreakTo $ breakTo ctInfo
  return $ [Label l1] ++ a1 ++ a2 ++ [Inst "cmpq $0, %rax", Inst $ "jne " ++ l1] ++ [Label l2]
emitStmt (For me1 (Just e2) me3 stmt) = do
  ctInfo <- get
  let l1 = makeLabel $ labelCount ctInfo
  let l2 = makeLabel $ 1+labelCount ctInfo
  let l3 = makeLabel $ 2+labelCount ctInfo
  void $ incrLabelCount
  void $ incrLabelCount
  void $ incrLabelCount
  void $ setBreakTo l2
  void $ setContinueTo l3
  a1 <- emitExpMaybe me1
  a2 <- emitExp e2
  a3 <- emitExpMaybe me3
  a4 <- emitStmt stmt
  void $ resetContinueTo $ continueTo ctInfo
  void $ resetBreakTo $ breakTo ctInfo
  return $ a1 ++ [Label l1] ++ a2 ++ [Inst "cmpq $0, %rax", Inst $ "je " ++ l2] ++ a4 ++ [Label l3]++ a3 ++ [Inst $ "jmp " ++ l1] ++ [Label l2]
emitStmt (For me1 Nothing me3 stmt) = do
  ctInfo <- get
  let l1 = makeLabel $ labelCount ctInfo
  let l2 = makeLabel $ 1+labelCount ctInfo
  let l3 = makeLabel $ 2+labelCount ctInfo
  void $ incrLabelCount
  void $ incrLabelCount
  void $ incrLabelCount
  void $ setBreakTo l2
  void $ setContinueTo l3
  a1 <- emitExpMaybe me1
  a3 <- emitExpMaybe me3
  a4 <- emitStmt stmt
  void $ resetContinueTo $ continueTo ctInfo
  void $ resetBreakTo $ breakTo ctInfo
  return $ a1 ++ [Label l1] ++ a4 ++ [Label l3]++ a3 ++ [Inst $ "jmp " ++ l1] ++ [Label l2]
emitStmt (ForDecl dcl (Just e1) me2 stmt) = do
  oldCTInfo <- get
  let l1 = makeLabel $ labelCount oldCTInfo
  let l2 = makeLabel $ 1+labelCount oldCTInfo
  let l3 = makeLabel $ 2+labelCount oldCTInfo
  void $ incrLabelCount
  void $ incrLabelCount
  void $ incrLabelCount
  void $ putNewScope
  void $ setBreakTo l2
  void $ setContinueTo l3
  asmDecl <- emitDeclaration dcl
  asmE1 <- emitExp e1
  asmE2 <- emitExpMaybe me2
  asmStmt <- emitStmt stmt
  newCTInfo <- get
  put $ CTInfo { env = env oldCTInfo, si = si oldCTInfo, labelCount = labelCount newCTInfo, currentScope = currentScope oldCTInfo, continueTo = continueTo oldCTInfo, breakTo = breakTo oldCTInfo}
  return $ asmDecl ++ [Label l1] ++ asmE1 ++ [ Inst "cmpq $0, %rax", Inst $ "je " ++ l2] ++ asmStmt ++ [Label l3]++ asmE2 ++ [Inst $ "jmp " ++ l1, Label l2, Inst "pop %rax"]
emitStmt (ForDecl dcl Nothing me2 stmt) = do
  oldCTInfo <- get
  let l1 = makeLabel $ labelCount oldCTInfo
  let l2 = makeLabel $ 1+labelCount oldCTInfo
  let l3 = makeLabel $ 2+labelCount oldCTInfo
  void $ incrLabelCount
  void $ incrLabelCount
  void $ incrLabelCount
  void $ putNewScope
  void $ setBreakTo l2
  void $ setContinueTo l3
  asmDecl <- emitDeclaration dcl
  asmE2 <- emitExpMaybe me2
  asmStmt <- emitStmt stmt
  newCTInfo <- get
  put $ CTInfo { env = env oldCTInfo, si = si oldCTInfo, labelCount = labelCount newCTInfo, currentScope = currentScope oldCTInfo, continueTo = continueTo oldCTInfo, breakTo = breakTo oldCTInfo}
  return $ asmDecl ++ [Label l1] ++ asmStmt ++[Label l3] ++asmE2 ++ [Inst $ "jmp " ++ l1, Label l2, Inst "pop %rax"]
emitStmt Break = do
  ctInfo <- get
  case breakTo ctInfo of
    Just label -> return $ [ Inst $ "jmp " ++ label]
    Nothing -> throwE "break in outside of any loop"
emitStmt Continue = do
  ctInfo <- get
  case continueTo ctInfo of
    Just label -> return $ [ Inst $ "jmp " ++ label]
    Nothing -> throwE "continue in outside of any loop"

emitExpMaybe :: Maybe Exp -> Compile
emitExpMaybe Nothing = return []
emitExpMaybe (Just e) = emitExp e

emitExp :: Exp -> Compile
emitExp (Const i) = return [ Inst $ "movq $" ++ show i ++ ", %rax" ]
emitExp (Unary ArithNeg e) = do
  a <- (emitExp e)
  return $ a ++ [ Inst "neg %rax" ]
emitExp (Unary BitCompl e) = do
  a <- (emitExp e)
  return $ a ++ [ Inst "not %rax" ]
emitExp (Unary LogNeg e) = do
  a <- (emitExp e)
  return $ a ++ [ Inst "cmpq $0, %rax", Inst "movq $0, %rax", Inst "sete %al"]
emitExp (Binary Plus e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "addq %rcx, %rax" ]
emitExp (Binary Minus e1 e2) = do
  a2 <- (emitExp e2)
  a1 <- (emitExp e1)
  return $ a2 ++ [ Inst "push %rax" ] ++ a1 ++ [ Inst "pop %rcx", Inst "subq %rcx, %rax" ] -- e1 - e2
emitExp (Binary Mul e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "imul %rcx, %rax" ]
emitExp (Binary Div e1 e2) = do
  a2 <- (emitExp e2)
  a1 <- (emitExp e1)
  return $ a2 ++ [ Inst "push %rax" ] ++ a1 ++ [ Inst "pop %rcx", Inst "xor %rdx, %rdx", Inst "idivq %rcx" ]
emitExp (Binary OpLT e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq %rax, %rcx", Inst "movq $0, %rax", Inst "setl %al" ] -- e1 < e2
emitExp (Binary OpGT e1 e2) = do
  a1 <- (emitExp e1) 
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq %rax, %rcx", Inst "movq $0, %rax", Inst "setg %al" ]
emitExp (Binary OpLTE e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq %rax, %rcx", Inst "movq $0, %rax", Inst "setle %al" ]
emitExp (Binary OpGTE e1 e2) = do
  a1 <- emitExp e1
  a2 <- emitExp e2
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq %rax, %rcx", Inst "movq $0, %rax", Inst "setge %al" ]
emitExp (Binary OpEQ e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq %rax, %rcx", Inst "movq $0, %rax", Inst "sete %al" ]
emitExp (Binary OpNEQ e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq %rax, %rcx", Inst "movq $0, %rax", Inst "setne %al" ]
emitExp (Binary LAND e1 e2) = do
  a1 <- (emitExp e1)
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "cmpq $0, %rcx", Inst "movq $0, %rcx", Inst "setne %cl", Inst "cmpq $0, %rax", Inst "movq $0, %rax", Inst "setne %al", Inst "andb %cl, %al" ]
emitExp (Binary LOR e1 e2) = do
  a1 <- (emitExp e1) 
  a2 <- (emitExp e2)
  return $ a1 ++ [ Inst "push %rax" ] ++ a2 ++ [ Inst "pop %rcx", Inst "orq %rcx, %rax", Inst "movq $0, %rax", Inst "setne %al" ]
emitExp (Binary MOD e1 e2) = do
  a2 <- (emitExp e2)
  a1 <- (emitExp e1)
  return $ a2 ++ [ Inst "push %rax" ] ++ a1 ++ [ Inst "pop %rcx", Inst "xor %rdx, %rdx", Inst "idivq %rcx", Inst "movq %rdx, %rax" ]
emitExp (Assignment var exp) = do
  ctinfo <- get
  let ctenv = env ctinfo
  let found = Map.lookup var ctenv
  a <- emitExp exp
  case found of
    Just offset -> return $ a ++ [Inst $ "movq %rax, " ++ show offset ++ "(%rbp)"]
    Nothing -> throwE ("assignment: variable not found: " ++ var)
emitExp (Var var) = do
  ctinfo <- get
  let ctenv = env ctinfo
  let found = Map.lookup var ctenv
  case found of
    Just offset -> return $ [Inst $ "movq " ++ show offset ++ "(%rbp), " ++ "%rax"]
    Nothing -> throwE ("variable reference: variable not found: " ++ var)
emitExp (ConditionalExp e1 e2 e3) = do
  ctinfo <- get
  let lc1 = labelCount ctinfo
  let lc2 = 1 + labelCount ctinfo
  let l1 = makeLabel lc1
  let l2 = makeLabel lc2
  void $ incrLabelCount
  void $ incrLabelCount
  a1 <- emitExp e1
  a2 <- emitExp e2
  a3 <- emitExp e3
  return $ a1 ++ [Inst $ "cmpq $0, %rax", Inst $ "je " ++ l1] ++ a2 ++ [ Inst $ "jmp " ++ l2 ] ++ [ Label l1 ] ++ a3 ++ [ Label l2 ]
emitExp (Funcall fname args) = do
  let bytesToRemove = wordsize * (length args)
  let n = wordsize*(1+length args)
  let pro = [Inst "movq %rsp, %rax", Inst $ "subq $" ++ show n ++ ", %rax", Inst "xorq %rdx, %rdx", Inst $ "movq $16, %rcx", Inst "idivq %rcx", Inst "subq %rdx, %rsp", Inst "pushq %rdx"]
  as <- emitArgs args
  return $ pro ++ as ++ [Inst $ "call " ++ fname, Inst $ "addq $" ++ show bytesToRemove ++ ", %rsp", Inst "pop %rdx", Inst "addq %rdx, %rsp"]

emitArgs :: [Exp] -> Compile
emitArgs [] = return []
emitArgs (h:rest) = do
  a <- emitExp h
  as <- emitArgs rest
  return $ a ++ [Inst "pushq %rax"] ++ as

getRealAsm :: Asm -> String
getRealAsm (Directive d) = "\t" ++ d
getRealAsm (Label l) = l ++ ":"
getRealAsm (Inst i) = "\t" ++ i

compile :: Prog -> Either String String
compile p = let res = emit p
            in case res of
                Right asm -> Right $ (intercalate "\n" $ map getRealAsm asm) ++ "\n"
                Left e -> Left e
