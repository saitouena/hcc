module Parser
  (
    parseC,
    parseExp, -- for debug
    parseCFromFile, -- for debug
    parseWithEof, -- for debug
    parseFundecl, -- for debug
    parseArgs, -- for debug
    parseFactor, -- for debug
    UOp (..),
    BOp (..),
    Exp (..),
    Stmt (..),
    Declaration (..),
    BlockItem (..),
    Fundecl (..),
    Prog (..),
  ) where

import Text.Parsec (parse, ParseError)
import Text.Parsec.String (Parser)
import Text.Parsec.Char (string,digit,satisfy,char,spaces,space, oneOf)
import Text.Parsec.Combinator (many1,eof)
import Text.Parsec.Prim (many,try, (<|>))
import Data.Char (isLetter, isDigit)
import Control.Monad (void)

-- utils

-- from http://jakewheat.github.io/intro_to_parsing/#getting-started
lexeme :: Parser a -> Parser a
lexeme p = do
           x <- p
           spaces
           return x

data UOp = ArithNeg | BitCompl | LogNeg deriving (Show)

data BOp = Minus | Plus | Mul | Div | OpLT | OpGT | OpLTE | OpGTE | OpEQ | OpNEQ | LAND | LOR | MOD deriving (Show)

data Exp = Unary UOp Exp
          | Const Int
          | Binary BOp Exp Exp
          | Assignment String Exp
          | Var String
          | ConditionalExp Exp Exp Exp
          | Funcall String [Exp]
  deriving (Show)

data Stmt = Return Exp
          | StmtExp (Maybe Exp) -- cf. 1; this is a valid C statement.
          | ConditionalStmt Exp Stmt (Maybe Stmt)
          | Compound [BlockItem]
          | For (Maybe Exp) (Maybe Exp) (Maybe Exp) Stmt
          | ForDecl Declaration (Maybe Exp) (Maybe Exp) Stmt
          | While Exp Stmt
          | Do Stmt Exp
          | Break
          | Continue
  deriving (Show)

data Declaration = Declaration String (Maybe Exp)
  deriving (Show)

data BlockItem = ItemStmt Stmt
                | ItemDeclaration Declaration
                deriving (Show)

data Fundecl = Function String [String] (Maybe [BlockItem]) -- function name, args, body
  deriving (Show)

data Prog = Prog [Fundecl]
  deriving (Show)

parseExp :: Parser Exp
parseExp = do
  try parseAssign <|> parseConditionalExp
  -- TODO: try is needed because <id> can be <id> = <exp> (Assignment) or <id> (Exp) and we can know it is Assignment when getting "=".
  -- try is considered harmful. how can I remove it?
  where parseAssign = do
          identifier <- lexeme $ parseIdentifier
          void $ lexeme $ char '='
          e <- parseExp
          return $ Assignment identifier e

parseExpMaybe :: Parser (Maybe Exp)
parseExpMaybe = do
  try p <|> (return Nothing)
  where p = do
          e <- parseExp
          return $ Just e

parseConditionalExp :: Parser Exp
parseConditionalExp = do
  try p <|> parseLogicalOrExp
  where p = do
          e1 <- lexeme $ parseLogicalOrExp
          void $ lexeme $ char '?'
          e2 <- lexeme $ parseExp
          void $ lexeme $ char ':'
          e3 <- lexeme $ parseConditionalExp
          return $ ConditionalExp e1 e2 e3

parseLogicalOrExp :: Parser Exp
parseLogicalOrExp = do
  t1 <- parseLogicalAndExp
  loop t1
  where termSuffix t1 = do
          void $ lexeme $ string "||"
          t2 <- parseLogicalAndExp
          loop (Binary LOR t1 t2)
        loop t = termSuffix t <|> return t

parseLogicalAndExp :: Parser Exp
parseLogicalAndExp = do
  t1 <- parseEqualityExp
  loop t1
  where termSuffix t1 = do
          void $ lexeme $ string "&&"
          t2 <- parseEqualityExp
          loop (Binary LAND t1 t2)
        loop t = termSuffix t <|> return t

parseEqualityExp :: Parser Exp
parseEqualityExp = do
  t1 <- parseRelationalExp
  loop t1
  where termSuffix t1 = do
          op <- lexeme $ (string "!=" <|> string "==")
          t2 <- parseRelationalExp
          case op of
            "!=" -> loop $ Binary OpNEQ t1 t2
            "==" -> loop $ Binary OpEQ t1 t2
            _ -> undefined
        loop t = termSuffix t <|> return t

parseRelationalExp :: Parser Exp
parseRelationalExp = do
  t1 <- parseAdditiveExp
  loop t1
  where termSuffix t1 = do
          op <- lexeme $ try (string "<=") <|> try (string ">=") <|> string "<" <|> string ">"
          -- TODO: I can remove try? (I think I can't)
          t2 <- parseAdditiveExp
          case op of
            "<=" -> loop $ Binary OpLTE t1 t2
            ">=" -> loop $ Binary OpGTE t1 t2
            "<" -> loop $ Binary OpLT t1 t2
            ">" -> loop $ Binary OpGT t1 t2
            _ -> undefined
        loop t = termSuffix t <|> return t

parseAdditiveExp :: Parser Exp
parseAdditiveExp = do
  t1 <- parseTerm
  loop t1
  where termSuffix t1 = do
          op <- lexeme $ oneOf "+-"
          t2 <- parseTerm
          case op of
            '+' -> loop (Binary Plus t1 t2)
            '-' -> loop (Binary Minus t1 t2)
            _ -> undefined
        loop t = termSuffix t <|> return t

parseTerm :: Parser Exp
parseTerm = do
  f1 <- parseFactor
  loop f1
  where factorSuffix f1 = do
          op <- lexeme $ oneOf "*/%"
          f2 <- parseFactor
          case op of
            '*' -> loop (Binary Mul f1 f2)
            '/' -> loop (Binary Div f1 f2)
            '%' -> loop (Binary MOD f1 f2)
            _ -> undefined
        loop t = factorSuffix t <|> return t

parseFactor :: Parser Exp
parseFactor = try parseFuncall <|> parseConst <|> parseParen <|> parseUnary <|> parseVarRef

parseFuncall :: Parser Exp
parseFuncall = do
  identifier <- lexeme $ parseIdentifier
  void $ lexeme $ char '('
  args <- try parseFuncallArgs <|> (return [])
  void $ lexeme $ char ')'
  return $ Funcall identifier args
  where parseFuncallArgs = do
          e <- lexeme $ parseExp
          loop [e] <|> return [e]
        loop acc = do
          void $ lexeme $ char ','
          e <- lexeme $ parseExp
          let next = e:acc
          loop next <|> return next

parseParen :: Parser Exp
parseParen = do
  void $ lexeme $ char '('
  e <- parseExp
  void $ lexeme $ char ')'
  return e

parseUnary :: Parser Exp
parseUnary = do
  op <- lexeme $ oneOf "!~-"
  f <- parseFactor
  case op of
    '!' -> return $ Unary LogNeg f
    '~' -> return $ Unary BitCompl f
    '-' -> return $ Unary ArithNeg f
    _ -> undefined

parseConst :: Parser Exp
parseConst = do
  i <- lexeme $ many1 digit
  return (Const $ read i)

parseVarRef :: Parser Exp
parseVarRef = do
  identifier <- lexeme $ parseIdentifier
  return $ Var identifier

parseProg :: Parser Prog
parseProg = do
  spaces
  fndcls <- many (lexeme $ parseFundecl)
  spaces
  return $ Prog fndcls

-- from http://jakewheat.github.io/intro_to_parsing/ 3.2 var parser.
parseIdentifier :: Parser String
parseIdentifier = do
  fc <- firstChar
  rest <- many nonFirstChar
  return (fc:rest)
  where
    firstChar = satisfy isLetter
    nonFirstChar = satisfy (\a -> isDigit a || isLetter a || a == '_')

parseFundecl :: Parser Fundecl
parseFundecl = do
  void $ string "int"
  void $ space
  void $ spaces
  funname <- lexeme parseIdentifier
  void $ lexeme $ char '('
  args <- lexeme $ (parseArgs <|> (return []))
  void $ lexeme $ char ')'
  body <- parseFundeclBody
  return $ Function funname args body

parseFundeclBody :: Parser (Maybe [BlockItem])
parseFundeclBody = try parseBody <|> ((lexeme $ char ';') >> return Nothing)
  where parseBody = do
          void $ lexeme $ char '{'
          items <- many parseBlockItem
          void $ lexeme $ char '}'
          return $ Just items

-- parseArg1 :: Parser [String]
-- parseArg1 = do
--   void $ string "int"
--   void $ space
--   void $ spaces
--   a <- parseIdentifier
--   return [a]

parseArgs :: Parser [String]
parseArgs = do
  void $ string "int"
  void $ space
  void $ spaces
  arg1 <- lexeme $ parseIdentifier
  loop [arg1] <|> return [arg1]
  where loop args = do
          void $ lexeme $ char ','
          void $ string "int"
          void $ space
          void $ spaces
          arg <- lexeme $ parseIdentifier
          let next = arg:args
          loop next <|> (return $ reverse next)

parseBlockItem :: Parser BlockItem
parseBlockItem = try parseItemDeclaration <|> parseItemStmt
-- try is needed beause parser needs to backtrack when it fails with 'n' of "int". There could be "if" maybe.

parseItemDeclaration :: Parser BlockItem
parseItemDeclaration = do
  dcl <- parseDeclaration
  return $ ItemDeclaration dcl

parseItemStmt :: Parser BlockItem
parseItemStmt = do
  stmt <- parseStmt
  return $ ItemStmt stmt

parseStmt :: Parser Stmt
parseStmt = try parseCompound
          <|> try parseReturn
          <|> try parseConditionalStmt
          <|> try parseFor
          <|> try parseWhile
          <|> try parseDo
          <|> try parseBreak
          <|> try parseContinue
          <|> parseStmtExp

parseCompound :: Parser Stmt
parseCompound = do
  void $ lexeme $ char '{'
  items <- many parseBlockItem
  void $ lexeme $ char '}'
  return $ Compound items

parseReturn :: Parser Stmt
parseReturn = do
  void $ string "return"
  void $ space -- need at least one space
  spaces
  e <- parseExp
  spaces
  void $ char ';'
  spaces
  return $ Return e

parseConditionalStmt :: Parser Stmt
parseConditionalStmt = do
  void $ lexeme $ string "if"
  void $ lexeme $ char '('
  cond <- lexeme $ parseExp
  void $ lexeme $ char ')'
  thenStmt <- lexeme $ parseStmt
  maybeSuffix cond thenStmt <|> noSuffix cond thenStmt
  where maybeSuffix cond thenStmt = do
          void $ lexeme $ string "else"
          elseStmt <- lexeme $ parseStmt
          return $ ConditionalStmt cond thenStmt (Just elseStmt)
        noSuffix cond thenStmt = do
          return $ ConditionalStmt cond thenStmt Nothing

parseStmtExp :: Parser Stmt
parseStmtExp = do
  e <- lexeme $ parseExpMaybe
  void $ lexeme $ char ';'
  return $ StmtExp e

parseDeclaration :: Parser Declaration
parseDeclaration = do
  void $ string "int"
  void $ space
  void $ spaces
  identifier <- lexeme $ parseIdentifier
  maybeSuffix identifier <|> noSuffix identifier
  where maybeSuffix identifier = do
          void $ lexeme $ char '='
          e <- lexeme $ parseExp
          void $ lexeme $ char ';'
          return $ Declaration identifier $ Just e
        noSuffix identifier = do
          void $ lexeme $ char ';'
          return $ Declaration identifier Nothing

parseFor :: Parser Stmt
parseFor = do
  void $ lexeme $ string "for"
  void $ lexeme $ char '('
  try parseD <|> parseE
  where parseD = do
          dcl <- lexeme $ parseDeclaration
          e1 <- lexeme $ parseExpMaybe
          void $ lexeme $ char ';'
          e2 <- lexeme $ parseExpMaybe
          void $ lexeme $ char ')'
          stmt <- lexeme $ parseStmt
          return $ ForDecl dcl e1 e2 stmt
        parseE = do
          e1 <- lexeme $ parseExpMaybe
          void $ lexeme $ char ';'
          e2 <- lexeme $ parseExpMaybe
          void $ lexeme $ char ';'
          e3 <- lexeme $ parseExpMaybe
          void $ lexeme $ char ')'
          stmt <- lexeme $ parseStmt
          return $ For e1 e2 e3 stmt

parseWhile :: Parser Stmt
parseWhile = do
  void $ lexeme $ string "while"
  void $ lexeme $ char '('
  e <- lexeme $ parseExp
  void $ lexeme $ char ')'
  stmt <- lexeme $ parseStmt
  return $ While e stmt

parseDo :: Parser Stmt
parseDo = do
  void $ lexeme $ string "do"
  stmt <- lexeme $ parseStmt
  void $ lexeme $ string "while"
  e <- lexeme $ parseExp
  void $ lexeme $ char ';'
  return $ Do stmt e

parseBreak :: Parser Stmt
parseBreak = do
  void $ lexeme $ string "break"
  void $ lexeme $ char ';'
  return Break

parseContinue :: Parser Stmt
parseContinue = do
  void $ lexeme $ string "continue"
  void $ lexeme $ char ';'
  return Continue

parseC :: String -> Either ParseError Prog
parseC s = parseWithEof parseProg s

parseCFromFile :: String -> IO ()
parseCFromFile sourcename = do
  src <- readFile sourcename
  case parseC src of
    Right res -> putStrLn $ show res
    Left err -> putStrLn $ show err

-- for debug
-- from: http://jakewheat.github.io/intro_to_parsing/
parseWithEof :: Parser a -> String -> Either ParseError a
parseWithEof p = parse (p <* eof) ""
