# hcc
toy C compiler
# target
linux x86-64
# reference
https://norasandler.com/2017/11/29/Write-a-Compiler.html
# test
https://github.com/nlsandler/write_a_c_compiler
