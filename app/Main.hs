module Main where

import System.Environment (getArgs)
import System.FilePath.Posix (dropExtension)
import System.Process (system)
import Parser
import CodeGen

invoke :: String -> IO ()
invoke filename = do
  src <- readFile filename
  let base = dropExtension filename
  let dst = base ++ ".s"
  let cmd = "gcc -no-pie " ++ dst ++ " -o " ++ base
  case parseC src of
    Right res0 -> case compile res0 of
                  Right res1-> writeFile dst res1 >> system cmd >> return ()
                  Left err -> putStrLn $ show err
    Left err -> putStrLn $ show err

main :: IO ()
main = do
  args <- getArgs
  if length args == 0
    then putStrLn $ "pass c source path."
    else invoke $ args !! 0
